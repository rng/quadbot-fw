Quadbot
=======

An small servo powered 8-DOF quadruped. Built around an [STM32F042][1] and [HXT900 micro servos][2]. Includes a bytecode based Logo-esque language with REPL via USB.

![Quadbot](hw/quadbot.jpg)

__Hardware:__

- STM32F042K6 (ARM Cortex M0, USB, 32KB Flash, 6KB RAM)
- 8x servo joints
- 2x LED eyes
- 2x phototransitors
- USB CDC to host
- UART/GPIO/SPI Breakout
- Battery charger + sensor
- 1Ah battery

[Bill of materials](hw/bom.md)

__Firmware:__

- Superloop and debug interface ([main.c](src/main.c))
- Virtual Machine and Primitives ([vm.c](src/vm.c))
- Mark & Sweep GC ([mem.c](src/mem.c))
- Gait trajectory contol ([trajectory.c](src/trajectory.c))
- HW abstraction + drivers ([bot.c](src/bot.c), [servo.c](src/servo.c), [hal.c](src/hal.c))
- PS2 Controller interface ([ps2ctrl.c](src/ps2ctrl.c), [ps2ctrl.h](inc/ps2ctrl.h))

__Host:__

- Compiler / REPL ([slogo.py](slogo.py))
- Debug interface ([dbgif.py](tool/dbgif.py))
- Gait model ([model.py](tool/model.py))

Usage
----

    :::bash
    # install deps
    aptitude install openocd gcc-arm-none-eabi gdb-arm-none-eabi libnewlib-arm-none-eabi python-serial

    # build firmware
    make

    # debug firmware
    make debugserver &
    make debug

    # connect via USB and run REPL
    ./slogo.py

Examples
--------

    :::ruby
    # robot motion
    >> fd 1
    >> rt 90

    # loops
    >> repeat 4 {
    ..   fd 1 rt 90
    .. }

    # Simple arithmetic
    >> 1+2
    3

    # functions
    >> to sqr :x
    ..   (:x * :x)
    .. end

    >> sqr 4
    16

    # more complex functions
    >> to map :block :lst
    ..   ifelse count :lst {
    ..     make ? first :lst
    ..     fput (apply :block) map :block (rest :lst)
    ..   } {
    ..     []
    ..   }
    .. end

    >> map {:? * :?} [1 2 3 4]
    (1, 4, 9, 16)

    # back traces on error
    >> map {10 / :?} [1 2 4 0]
    Runtime exception: VM_EXC.DIV0
    Backtrace:
       repl | map { 10 / :? } list 1 2 4 0 4
        map | map :block rest :lst
        map | map :block rest :lst
        map | map :block rest :lst
        map | apply :block
       repl | 10 / :?

[1]: http://www.st.com/content/st_com/en/products/microcontrollers/stm32-32-bit-arm-cortex-mcus/stm32f0-series/stm32f0x2/stm32f042k6.html
[2]: https://hobbyking.com/en_us/hxt900-micro-servo-1-6kg-0-12sec-9g.html
