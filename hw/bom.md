Qty  |  Part                       |  Value  |  Vendor     |  Part #                  |  Refs
-----|-----------------------------|---------|-------------|--------------------------|-----------------------------------------------------------------
1    |  header-2x1_s2b-ph          |         |  Digikey    |  455-1719-ND             |  J12
1    |  header-5x2_header-5x2-127  |         |  Digikey    |  1175-1627-ND            |  J11
1    |  microb5_zx62-b-5pa         |         |  Digikey    |  H125270CT-ND            |  J1
1    |  stm32f042_lqfp32           |         |  Digikey    |  497-14647-ND            |  U1
1    |  mcp73831_sot23-5           |         |  Digikey    |  MCP73831T-2ATI/OTCT-ND  |  U3
1    |  mcp1755_sot23-5            |         |  Digikey    |  MCP1824T-3302E/OTCT-ND  |  U2
1    |  led_s1608                  |  green  |  Digikey    |  732-4971-1-ND           |  D1
1    |  led_s1608                  |  red    |  Digikey    |  SML-D12U1WT86CT-ND      |  D2
14   |  c_s1608                    |  0.1uF  |  Digikey    |  490-1524-1-ND           |  C1, C14, C15, C16, C17, C18, C19, C2, C20, C21, C22, C3, C5, C6
1    |  c_s1608                    |  1uF    |  Digikey    |  311-1446-1-ND           |  C13
3    |  c_s1608                    |  4.7uF  |  Digikey    |  1276-6462-1-ND          |  C4, C7, C8
1    |  c_s2012                    |  22uF   |  Digikey    |  490-10747-1-ND          |  C9
2    |  cpol_s3216                 |  100uF  |  Digikey    |  732-8598-1-ND           |  C25, C26
1    |  l_s1608                    |  33     |  Digikey    |  490-5220-1-ND           |  L2
2    |  r_s1608                    |  100    |  Digikey    |  311-100HRCT-ND          |  R13, R14
7    |  r_s1608                    |  10k    |  Digikey    |  311-10KGRCT-ND          |  R1, R11, R12, R15, R2, R5, R6
1    |  r_s1608                    |  20     |  Digikey    |  311-20GRCT-ND           |  R4
1    |  r_s1608                    |  2k     |  Digikey    |  RR08P2.0KDCT-ND         |  R3
1    |  r_s1608                    |  470    |  Digikey    |  311-470GRCT-ND          |  R7
2    |  5mm LED                    |  blue   |  Digikey    |  C503B-BCN-CV0Z0461-ND   |
2    |  3mm phototransistor        |         |  Digikey    |  475-1443-ND             |
1    |  Li-ion Battery             |  1 Ah   |  Sparkfun   |  PRT-13813               |
1    |  PCB                        |         |  OSH Park   |                          |
8    |  HXT900                     |         |  Hobbyking  |                          |
1    |  Lasercut legs              |         |  Ponoko     |                          |
