import struct
import serial
import enum


class DBG_CMD(enum.IntEnum):
  VM_READ_RSTACK = 0
  VM_READ_HEAP = 1
  VM_LOAD = 2
  VM_RUN = 3
  VM_STOP = 4
  VM_STATE = 5
  VM_RESULT = 6
  VM_EXCEPTION = 7
  VM_RESET = 8


class VM_EXC(enum.IntEnum):
  NONE = 0
  OVERFLOW = 1
  UNDERFLOW = 2
  UNKNOWN_VAR = 3
  UNKNOWN_PRIM = 4
  UNKNOWN_INST = 5
  TOO_MANY_VAR = 6
  INVALID_TYPE = 7
  LIST_BOUNDS = 8
  OUT_OF_MEM = 9
  DIV0 = 10

class MEM_TAG(enum.IntEnum):
  INT  = 0
  BLK  = 1
  PAIR = 2


# Pretty-printer for tagged VM objects (see mem.h)
def obj_decode(dbgif, obj):
  def _helper(o):
    tag = o & 2
    val = o >> 3
    if tag == MEM_TAG.INT:
      return val
    elif tag == MEM_TAG.BLK:
      return "<block %d>" % val
    elif tag == MEM_TAG.PAIR:
      # nil value
      if val == 0:
        return None
      # actual pair
      pair = dbgif.heap_read(o)
      cdr = pair >> 16
      car = pair & 0xff
      return (_helper(car), _helper(cdr))
    else:
      raise Exception("unhandled obj tag %d" % tag)

  # turn a linked-list of pairs into a plain list
  # (1, (2, (3, nil))) -> (1,2,3)
  def _listify(obj):
    if isinstance(obj[1], tuple):
      return (obj[0],) + _listify(obj[1])
    elif obj[1] is None:
      return (obj[0],)
    return obj

  decoded = _helper(obj)
  if isinstance(decoded, tuple):
    return _listify(decoded)
  elif decoded is None:
    return ()
  return decoded

 
class DebugBase(object):
  def load(self, code, addr): pass
  def run(self, addr): pass
  def stop(self): pass
  def reset(self): pass
  def running(self): return False
  def exception(self): return VM_EXC.NONE
  def result(self): return None
  def heap_read(self, obj): return None
  def rstack_read(self, idx): return None


class DebugUSB(DebugBase):
  def __init__(self, portname="/dev/ttyACM0"):
    self.portname = portname

  def _sendrecv(self, outdata, inlen=0):
      port = serial.Serial(self.portname, 230400)
      port.write(outdata)
      if inlen:
        return bytearray(port.read(inlen))

  def load(self, code, addr):
    s = struct.pack("bbb", DBG_CMD.VM_LOAD, addr, len(code))
    s += str(bytearray(code))
    self._sendrecv(s)

  def run(self, addr):
    s = struct.pack("bb", DBG_CMD.VM_RUN, addr)
    self._sendrecv(s)

  def stop(self):
    s = struct.pack("b", DBG_CMD.VM_STOP)
    self._sendrecv(s)

  def reset(self):
    s = struct.pack("b", DBG_CMD.VM_RESET)
    self._sendrecv(s)

  def running(self):
    s = struct.pack("b", DBG_CMD.VM_STATE)
    return self._sendrecv(s, 1)[0]

  def exception(self):
    s = struct.pack("b", DBG_CMD.VM_EXCEPTION)
    return VM_EXC(self._sendrecv(s, 1)[0])

  def result(self):
    s = struct.pack("b", DBG_CMD.VM_RESULT)
    val = struct.unpack("i", self._sendrecv(s, 4))[0]
    if val != 0xbadc0de:
      return obj_decode(self, val)
  
  def heap_read(self, obj):
    s = struct.pack(">bH", DBG_CMD.VM_READ_HEAP, obj)
    return struct.unpack("I", self._sendrecv(s, 4))[0]

  def rstack_read(self, idx):
    s = struct.pack("bb", DBG_CMD.VM_READ_RSTACK, idx)
    val = struct.unpack("i", self._sendrecv(s, 4))[0]
    if val != 0xbadc0de:
      return val
 

def debugif(name):
  return {
    "usb" : DebugUSB,
  }[name]()
