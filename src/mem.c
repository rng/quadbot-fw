#include "mem.h"
#include "common.h"
#include <string.h>

typedef struct {
  uint16_t *heap;
  size_t heap_size;
  mem_val_t free_list;
  mem_root_visit_cb_t root_visit;
  void *root_ctx;
  int free;
} mem_priv_t;

mem_priv_t mem_priv;

#if 0
#include <stdio.h>

static void mem_dump(void)
{
  printf("dump:\n");
  for (int i=0;i<mem_priv.heap_size;i++) {
    printf("%04x%s ", mem_priv.heap[i], (mem_priv.heap[i]&MEM_TAG_MARK) ? "!" : " ");
    if (i%16 == 15) printf("\n");
  }
  printf("free: %d\n", mem_priv.free_list);
  printf("\n");
}
#endif 

static void mem_set(mem_val_t obj, uint32_t idx, mem_val_t val);

void mem_init(uint8_t *heap, size_t heap_size,
              mem_root_visit_cb_t root_visit, void *root_ctx)
{
  memset(heap, 0, heap_size);
  mem_priv.heap = (uint16_t*)heap;
  mem_priv.heap_size = heap_size/2;
  mem_priv.root_visit = root_visit;
  mem_priv.root_ctx = root_ctx;
  mem_priv.free_list = NIL;
  // Construct the free list. Initially this is just a link list of all pairs
  // in the heap. Each time we allocate a pair we pop the first link off the
  // free list chain.
  uint16_t obj = mem_priv.heap_size;
  while (obj>2) {
    obj -= 2;
    mem_val_t pair = MK_TAGGED(obj, MEM_TAG_PAIR);
    mem_set(pair, 0, mem_priv.free_list);
    mem_priv.free_list = pair;
    mem_priv.free++;
  }
}

// Core garbage collection (naive mark and sweep)

#define MARK(ofs) mem_priv.heap[ofs] |= MEM_TAG_MARK
#define IS_MARKED(ofs) (mem_priv.heap[ofs] & MEM_TAG_MARK)

void mem_mark(mem_val_t val)
{
  if (TAGGED_TAG(val) != MEM_TAG_PAIR) {
    return;
  }
  uint16_t ofs = TAGGED_VAL(val);
  if (IS_MARKED(ofs)) {
      return;
  }
  MARK(ofs);
  // recurse
  mem_mark(mem_priv.heap[ofs]);
  mem_mark(mem_priv.heap[ofs+1]);
}

static void mem_sweep(void)
{
  uint16_t ofs = 2;
  while (ofs < mem_priv.heap_size) {
    if (!IS_MARKED(ofs)) {
      // not marked - it's unused, so add it back to the free list
      mem_val_t pair = MK_TAGGED(ofs, MEM_TAG_PAIR);
      mem_set(pair, 0, mem_priv.free_list);
      mem_priv.free_list = pair;
      mem_priv.free++;
    } else {
      // marked (still referenced), clear mark bit
      mem_priv.heap[ofs] &= ~MEM_TAG_MARK;
    }
    ofs += 2;
  }
}

static void mem_gc(void)
{
  // start marking from roots
  mem_priv.root_visit(mem_priv.root_ctx);
  // sweep and add to free list
  mem_sweep();
}

// Allocator

mem_val_t mem_alloc()
{
  // No free list, we're out of mem, so GC
  if (mem_priv.free_list == NIL) {
    mem_gc();
    // out of mem even after GC
    if (mem_priv.free_list == NIL) {
      return NIL;
    }
  }
  // pop head off free list and return it
  mem_val_t obj = mem_priv.free_list;
  mem_priv.free_list = mem_car(mem_priv.free_list);
  mem_priv.free--;
  return obj;
}

// Pair construction and accessors

static void mem_set(mem_val_t obj, uint32_t idx, mem_val_t val)
{
  ASSERT(idx <= 2);
  uint32_t obj_base = TAGGED_VAL(obj);
  ASSERT((obj_base + idx) < mem_priv.heap_size);
  mem_priv.heap[obj_base + idx] = val;
}

static mem_val_t mem_get(mem_val_t obj, uint32_t idx)
{
  ASSERT(idx <= 2);
  uint32_t obj_base = TAGGED_VAL(obj);
  ASSERT((obj_base + idx) < mem_priv.heap_size);
  return mem_priv.heap[obj_base + idx];
}

mem_val_t mem_pair(mem_val_t car, mem_val_t cdr)
{
  mem_val_t pair = mem_alloc();
  if (pair) {
    mem_set(pair, 0, car);
    mem_set(pair, 1, cdr);
  }
  return pair;
}

mem_val_t mem_car(mem_val_t pair)
{
  return mem_get(pair, 0);
}

mem_val_t mem_cdr(mem_val_t pair)
{
  return mem_get(pair, 1);
}

void mem_set_car(mem_val_t pair, mem_val_t val)
{
  mem_set(pair, 0, val);
}

void mem_set_cdr(mem_val_t pair, mem_val_t val)
{
  mem_set(pair, 1, val);
}

uint32_t mem_read_raw(mem_val_t obj)
{
  uint32_t obj_base = TAGGED_VAL(obj);
  ASSERT((obj_base + 1) <= mem_priv.heap_size);
  return *(uint32_t*)(&mem_priv.heap[obj_base]);
}
