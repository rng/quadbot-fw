#include "stm32f0xx_hal.h"

#define PS2CTRL_IMPL
#include "ps2ctrl.h"

extern SPI_HandleTypeDef hspi1;

// Toggle controller ATTN line (aka SPI chip-select)
static void attn(bool v)
{
  HAL_GPIO_WritePin(SPI_CS_GPIO_Port, SPI_CS_Pin, !v);
}

// Trimmed down version of HAL_SPI_TransmitReceive for single byte transfers
static uint8_t spi_txrx(SPI_HandleTypeDef *hspi, uint8_t t)
{
  SET_BIT(hspi->Instance->CR2, SPI_RXFIFO_THRESHOLD);
  // enable if needed
  if((hspi->Instance->CR1 &SPI_CR1_SPE) != SPI_CR1_SPE) {
    __HAL_SPI_ENABLE(hspi);
  }
  // wait for tx
  while ((hspi->Instance->SR & SPI_FLAG_TXE) != SPI_FLAG_TXE);
  *(__IO uint8_t *)&hspi->Instance->DR = t;
  // wait for rx
  while ((hspi->Instance->SR & SPI_FLAG_RXNE) != SPI_FLAG_RXNE);
  return *(__IO uint8_t *)&hspi->Instance->DR;
}

static void udelay(int n)
{
    for (int j=0;j<n*8;j++) {
      __ASM("nop;");
    }
}

// Transfer a buffer out to the controller and read response.
// The controllers require a delay between each byte of the transfer, which
// means we can't easily call HAL_SPI_TransmitReceive on the entire buffer.
void ps2ctrl_sendrecv(uint8_t *tbuf, uint8_t *rbuf, size_t buf_len)
{
  attn(1);
  udelay(5);
  for (int i=0;i<buf_len;i++) {
    rbuf[i] = spi_txrx(&hspi1, tbuf[i]);
    udelay(10);
  }
  udelay(5);
  attn(0);
  udelay(50);
}


