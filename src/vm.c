#include <string.h>
#include "vm.h"
#include "common.h"
#include "bot.h"
#include "servo.h"
#include "slogo_rom.h"

// Opcodes and primitives
enum { OP_JF=0, OP_BLK, OP_PRIM, OP_IMM, OP_CALL, OP_TAIL, OP_RET, OP_DEF, OP_GET };
enum { PRIM_GT=0, PRIM_LT, PRIM_EQ, PRIM_GE, PRIM_LE, PRIM_NE, PRIM_SUB, PRIM_ADD,
       PRIM_DIV, PRIM_MUL,
       PRIM_HALT, PRIM_FWD, PRIM_BAK, PRIM_RGT, PRIM_LFT, PRIM_RAND,
       PRIM_JOINT, PRIM_ADC, PRIM_LED, PRIM_BEEP, PRIM_LIST, PRIM_ITEM, PRIM_COUNT,
       PRIM_APPLY, PRIM_WAIT, PRIM_MOTION, PRIM_PWR, PRIM_FIRST, PRIM_REST,
       PRIM_FPUT,
};

// Check a values tag, raise if incorrect
#define CHECK_TAG(tagged, tag) { \
  if (TAGGED_TAG(tagged) != tag) { \
    vm_raise(vm, VM_EXCEPT_INVALID_TYPE); \
  } \
}


void vm_gc_roots(void *ctx)
{
  vm_t *vm = (vm_t*)ctx;
  for (int i=0; i<vm->dstack.p; i++) {
    mem_mark(vm->dstack.d[i]);
  }
  for (int i=0; i<vm->env_top; i++) {
    mem_mark(vm->env[i].value);
  }
}

void vm_init(vm_t *vm)
{
  memset(vm, 0, sizeof(vm_t));
  mem_init(vm->heap, sizeof(vm->heap), vm_gc_roots, vm);
}

static mem_val_t vm_block(uint32_t v)
{
  return MK_TAGGED(v, MEM_TAG_BLK);
}

static mem_val_t vm_int(int32_t v)
{
  return MK_TAGGED((uint32_t)v, MEM_TAG_INT);
}

// Raise a runtime exception
static void vm_raise(vm_t *vm, vm_exception_t exception)
{
  longjmp(vm->except, exception);
}

// Data stack manipulation
static void push(vm_t *vm, mem_val_t val)
{
  if (vm->dstack.p >= VM_MAX_STACK) {
    vm_raise(vm, VM_EXCEPT_OVERFLOW);
  }
  vm->dstack.d[vm->dstack.p++] = val;
}

static mem_val_t pop(vm_t *vm)
{
  if (vm->dstack.p <= 0) {
    vm_raise(vm, VM_EXCEPT_UNDERFLOW);
  }
  return vm->dstack.d[--vm->dstack.p];
}

static int32_t pop_num(vm_t *vm)
{
  mem_val_t a = pop(vm);
  CHECK_TAG(a, MEM_TAG_INT);
  return TAGGED_VAL((int32_t)a);
}

static void push_num(vm_t *vm, int32_t v)
{
  push(vm, vm_int(v));
}

// Calls
static void vm_call(vm_t *vm, uint32_t target)
{
  if (vm->rstack.p >= VM_MAX_STACK) {
    vm_raise(vm, VM_EXCEPT_OVERFLOW);
  }
  // save pc and jump
  vm->rstack.d[vm->rstack.p++] = vm->pc;
  vm->pc = target;
  // save var pointer
  vm->rstack.d[vm->rstack.p++] = vm->env_top;
}

static void vm_ret(vm_t *vm)
{
  if (vm->rstack.p <= 0) {
    vm_raise(vm, VM_EXCEPT_UNDERFLOW);
  }
  // restore var pointer and pc
  vm->env_top = vm->rstack.d[--vm->rstack.p];
  vm->pc = vm->rstack.d[--vm->rstack.p];
}

// Variable declaration and access
static void vm_def(vm_t *vm, uint32_t id, mem_val_t val)
{
  if (vm->env_top >= VM_MAX_ENV) {
    vm_raise(vm, VM_EXCEPT_TOO_MANY_VAR);
  }
  vm->env[vm->env_top].id = id;
  vm->env[vm->env_top].value = val;
  vm->env_top++;
}

static mem_val_t vm_get(vm_t *vm, uint32_t id)
{
  int envp = vm->env_top-1;
  while (envp >= 0) {
    if (vm->env[envp].id == id) {
      return vm->env[envp].value;
    }
    envp--;
  }
  vm_raise(vm, VM_EXCEPT_UNKNOWN_VAR);
  // won't get here
  return 0;
}

// Lists
static mem_val_t list_new(vm_t *vm)
{
  int32_t length = pop_num(vm);

  mem_val_t list = NIL;
  for (int i=0; i<length; i++) {
    push(vm, list);
    list = mem_pair(NIL, NIL);
    mem_set_cdr(list, pop(vm));
    mem_set_car(list, pop(vm));
    if (!list) {
      vm_raise(vm, VM_EXCEPT_OUT_OF_MEM);
    }
  }
  return list;
}

static uint32_t list_count(vm_t *vm, mem_val_t list)
{
  uint32_t len = 0;
  CHECK_TAG(list, MEM_TAG_PAIR);
  while (list != NIL) {
    list = mem_cdr(list);
    len++;
  }
  return len;
}

static mem_val_t list_at(vm_t *vm, mem_val_t list, int32_t index)
{
  CHECK_TAG(list, MEM_TAG_PAIR);
  if (index < 0) {
    vm_raise(vm, VM_EXCEPT_LIST_BOUNDS);
  }
  while ((list != NIL) && (index > 0)) {
    list = mem_cdr(list);
    index--;
  }
  if (list == NIL) {
    vm_raise(vm, VM_EXCEPT_LIST_BOUNDS);
  }
  return mem_car(list);
}

static mem_val_t list_first(vm_t *vm, mem_val_t list)
{
  CHECK_TAG(list, MEM_TAG_PAIR);
  return mem_car(list);
}

static mem_val_t list_rest(vm_t *vm, mem_val_t list)
{
  CHECK_TAG(list, MEM_TAG_PAIR);
  return mem_cdr(list);
}

static mem_val_t list_fput(vm_t *vm, mem_val_t first, mem_val_t rest)
{
  CHECK_TAG(rest, MEM_TAG_PAIR);
  return mem_pair(first, rest);
}

// Primitives
#define BINOP(OP) { \
  int32_t a = pop_num(vm); \
  int32_t b = pop_num(vm); \
  push(vm, vm_int(b OP a)); \
}

#define POP2() b = pop_num(vm); a = pop_num(vm);

static void vm_prim_apply(vm_t *vm)
{
  mem_val_t block = pop(vm);
  CHECK_TAG(block, MEM_TAG_BLK);
  vm_call(vm, TAGGED_VAL(block));
}

static void vm_prim(vm_t *vm, uint32_t prim)
{
  int32_t a, b;
  switch (prim) {
      // binary ops
      case PRIM_GT:     BINOP(>); break;
      case PRIM_LT:     BINOP(<); break;
      case PRIM_EQ:     BINOP(==); break;
      case PRIM_GE:     BINOP(>=); break;
      case PRIM_LE:     BINOP(<=); break;
      case PRIM_NE:     BINOP(!=); break;
      case PRIM_SUB:    BINOP(-); break;
      case PRIM_ADD:    BINOP(+); break;
      case PRIM_MUL:    BINOP(*); break;
      case PRIM_DIV:
        POP2();
        if (b == 0) vm_raise(vm, VM_EXCEPT_DIV0);
        push(vm, vm_int(a / b));
        break;
      // misc
      case PRIM_HALT:   vm_stop(vm); break;
      case PRIM_APPLY:  vm_prim_apply(vm); break;
      // motion, one turn gait is ~ 15 degrees
      case PRIM_FWD:    bot_play_motion(TRAJ_FD, pop_num(vm)); break;
      case PRIM_BAK:    bot_play_motion(TRAJ_BK, pop_num(vm)); break;
      case PRIM_RGT:    bot_play_motion(TRAJ_RT, pop_num(vm)/15); break;
      case PRIM_LFT:    bot_play_motion(TRAJ_LT, pop_num(vm)/15); break;
      case PRIM_MOTION: bot_play_motion((trajectory_t)pop_num(vm), 1); break;
      // not really the best random
      case PRIM_RAND:   push_num(vm, bot_time() % pop_num(vm)); break;
      // misc HW
      case PRIM_ADC:    push_num(vm, bot_adc_read(pop_num(vm))); break;
      case PRIM_LED:    POP2(); bot_set_eye(a, b); break;
      case PRIM_BEEP:   POP2(); bot_beep(a*10, b*10, 2); break;
      case PRIM_JOINT:  POP2(); servo_set_angle(a, (int32_t)b); break;
      case PRIM_WAIT:   vm->wait_until = bot_time() + pop_num(vm)*100; break;
      case PRIM_PWR:   
        b = pop_num(vm);
        for (a=0;a<8;a++) { servo_enable(a, b); };
        break;
      // lists
      case PRIM_LIST:   push(vm, list_new(vm)); break;
      case PRIM_COUNT:  push_num(vm, list_count(vm, pop(vm))); break;
      case PRIM_ITEM:
        b = pop_num(vm);
        a = pop(vm);
        push(vm, list_at(vm, a, b));
        break;
      case PRIM_FIRST:  push(vm, list_first(vm, pop(vm))); break;
      case PRIM_REST:   push(vm, list_rest(vm, pop(vm))); break;
      case PRIM_FPUT:
        b = pop(vm);
        a = pop(vm);
        push(vm, list_fput(vm, a, b)); break;

      default:
        vm_raise(vm, VM_EXCEPT_UNKNOWN_PRIM);
        break;
  }
}

// Fetch an instruction byte (from either user code or ROM)
static uint8_t vm_fetch(vm_t *vm)
{
  uint8_t ins = 0;
  if (vm->pc < sizeof(vm->code)) {
    ins = vm->code[vm->pc];
  } else if (vm->pc >= VM_MAX_USER_CODE) {
    ins = slogo_rom[vm->pc - VM_MAX_USER_CODE];
  } else {
    vm_raise(vm, VM_EXCEPT_UNKNOWN_INST);
  }
  vm->pc++;
  return ins;
}

// Core interpreter
static void vm_interp1(vm_t *vm)
{
  uint8_t ins = vm_fetch(vm);
  uint8_t op = ins >> 4;
  uint8_t arg = ins & 0xf;
  if (arg == 15) {
    arg = vm_fetch(vm);
  }
  switch (op) {
    case OP_JF:   if (!pop_num(vm)) { vm->pc += arg; } break;
    case OP_BLK:  push(vm, vm_block(vm->pc)); vm->pc += arg; break;
    case OP_PRIM: vm_prim(vm, arg); break;
    case OP_IMM:  push(vm, vm_int(arg)); break;
    case OP_CALL: vm_call(vm, arg); break;
    case OP_TAIL: vm->pc = arg; break;
    case OP_RET:  vm_ret(vm); break;
    case OP_DEF:  vm_def(vm, arg, pop(vm)); break;
    case OP_GET:  push(vm, vm_get(vm, arg)); break;
    default:
      vm_raise(vm, VM_EXCEPT_UNKNOWN_INST);
      break;
  }
}

// Interpret a single VM instruction
void vm_interp(vm_t *vm)
{
  // Don't do anything if we're moving, not executing, or waiting
  if (bot_in_motion() || (!vm->running) ||
      (vm->wait_until > bot_time())) {
    return;
  }
  if (vm->pc < VM_MAX_CODE) {
    vm->exc = (vm_exception_t)setjmp(vm->except);
    if (!vm->exc) {
      // try to execute a single instruction
      vm_interp1(vm);
    } else {
      // catch exception and stop
      vm->running = false;
    }
  } else {
    vm_stop(vm);
  }
}

void vm_run(vm_t *vm, uint16_t start)
{
  ASSERT(!vm->running);
  vm->dstack.p = vm->rstack.p = vm->env_top = 0;
  vm->pc = start;
  vm->running = true;
}

void vm_stop(vm_t *vm)
{
  vm->exc = VM_EXCEPT_NONE;
  vm->running = false;
}

void vm_reset(vm_t *vm)
{
  memset(&vm->dstack, 0, sizeof(vm->dstack));
  memset(&vm->rstack, 0, sizeof(vm->rstack));
  memset(&vm->env, 0, sizeof(vm->env));
  vm->pc = 0;
  vm->env_top = 0;
  vm->wait_until = 0;
  vm->running = false;
  vm->exc = VM_EXCEPT_NONE;
  mem_init(vm->heap, sizeof(vm->heap), vm_gc_roots, vm);
}

bool vm_is_running(vm_t *vm)
{
  return vm->running;
}

uint32_t vm_get_result(vm_t *vm)
{
  if (vm->dstack.p > 0) {
    return pop(vm);
  }
  return 0xbadc0de;
}

uint32_t vm_get_rstack(vm_t *vm, size_t idx)
{
  if (idx < vm->rstack.p) {
    return vm->rstack.d[idx];
  } else if (idx == vm->rstack.p) {
    return vm->pc;
  } else if (idx == (size_t)(vm->rstack.p+1)) {
    return vm->env_top;
  }
  return 0xbadc0de;
}

vm_exception_t vm_exception(vm_t *vm)
{
  return vm->exc;
}
