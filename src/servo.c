#include "stm32f0xx_hal.h"
#include "servo.h"
#include "common.h"

extern TIM_HandleTypeDef htim3;
extern TIM_HandleTypeDef htim2;

typedef struct {
  int16_t joint_angle[8];
} servo_priv_t;

servo_priv_t servo_priv;

static const uint16_t joint_zero[] = {
  450, // 0 K back  right
  420, // 1 S back  right
  920, // 2 K front right
  530, // 3 S front right
  500, // 4 K front left
  500, // 5 S front left
  500, // 6 S back  left
  950, // 7 K back  left
};

static const int8_t joint_orientation[] = {
  -1, // kbr
  -1, // sbr
   1, // kfr
  -1, // sfr
  -1, // kfl
   1, // sfl
   1, // sbl
   1  // kbl
};

void servo_set_pos(int n, int pos)
{
  int val = pos;
  switch (n) {
    case 0: htim2.Instance->CCR1 = val; break;
    case 1: htim2.Instance->CCR2 = val; break;
    case 2: htim2.Instance->CCR3 = val; break;
    case 3: htim2.Instance->CCR4 = val; break;
    case 4: htim3.Instance->CCR1 = val; break;
    case 5: htim3.Instance->CCR2 = val; break;
    case 6: htim3.Instance->CCR3 = val; break;
    case 7: htim3.Instance->CCR4 = val; break;
  }
}

void servo_set_angle(int n, int angle)
{
  servo_priv.joint_angle[n] = angle;
  int orient = joint_orientation[n];
  int pos = joint_zero[n] + angle*orient;
  ASSERT((pos >= 300) && (pos <= 1100));
  servo_set_pos(n, pos);
}

void servo_lerp(int n, int end_angle, int ms)
{
  int start_angle = servo_priv.joint_angle[n];
  int d_angle = end_angle - start_angle;
  for (int i=0;i<ms;i++) {
    int angle = start_angle + (d_angle*i)/ms; 
    servo_set_angle(n, angle);
    HAL_Delay(1);
  }
}

void servo_enable(int n, bool on)
{
  int ch = 0;
  TIM_HandleTypeDef *tim = (n < 4) ? &htim2 : &htim3;
  switch (n) {
    case 0: ch = TIM_CHANNEL_1; break;
    case 1: ch = TIM_CHANNEL_2; break;
    case 2: ch = TIM_CHANNEL_3; break;
    case 3: ch = TIM_CHANNEL_4; break;
    case 4: ch = TIM_CHANNEL_1; break;
    case 5: ch = TIM_CHANNEL_2; break;
    case 6: ch = TIM_CHANNEL_3; break;
    case 7: ch = TIM_CHANNEL_4; break;
  }
  if (on) {
    HAL_TIM_PWM_Start(tim, ch);
  } else {
    HAL_TIM_PWM_Stop(tim, ch);
  }
}

