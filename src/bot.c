#include "stm32f0xx_hal.h"
#include "trajectory.h"
#include "common.h"
#include "servo.h"
#include "bot.h"
#include "notes.h"

extern ADC_HandleTypeDef hadc;
extern TIM_HandleTypeDef htim1;

typedef struct {
  int pos;
  bool in_motion;
  trajectory_t motion;
  uint8_t motion_count;
} bot_priv_t;

bot_priv_t bot_priv;

static uint32_t adc_read(ADC_HandleTypeDef* adc, uint32_t channel)
{
  uint32_t a, b, c;
  ASSERT(HAL_ADC_Start(adc) == HAL_OK);
  ASSERT(HAL_ADC_PollForConversion(adc, 1000) == HAL_OK);
  a = HAL_ADC_GetValue(adc);
  ASSERT(HAL_ADC_PollForConversion(adc, 1000) == HAL_OK);
  b = HAL_ADC_GetValue(adc);
  ASSERT(HAL_ADC_PollForConversion(adc, 1000) == HAL_OK);
  c = HAL_ADC_GetValue(adc);
  HAL_ADC_Stop(adc);
  switch (channel) {
    case 0: return a; break;
    case 1: return b; break;
    case 4: return c; break;
  }
  return -1;
}

uint32_t bot_adc_read(uint32_t channel)
{
  return adc_read(&hadc, channel);
}

void bot_init(void)
{
  bot_set_eye(LEFT|RIGHT,  1);
  for (int i=0;i<8;i++) {
    servo_enable(i, true);
    servo_set_pos(i, 0);
  }
  HAL_Delay(500);
  bot_set_eye(LEFT|RIGHT,  0);

  // zero all servos
  for (int i=0;i<8;i+=2) {
    servo_set_angle(i, 0);
    servo_set_angle(i+1, 0);
    HAL_Delay(500);
  }
  // say hi
  bot_play_motion(TRAJ_NOD, 1);

  HAL_GPIO_WritePin(SND_EN_GPIO_Port, SND_EN_Pin, (GPIO_PinState)1);
  HAL_Delay(100);
  // play startup tune
  uint16_t notes[] = { C5, G4, C5, E5, G5, C6, G5, Gs4, C5, Ds5, Gs5, Ds5, Gs5,
                       C6, Ds6, Gs6, Ds6, As4, D5, F5, As5, D6, F6, As6, F6};
  for (int i=0;i<sizeof(notes)/2;i++) {
    bot_beep(notes[i], 50, 2);
  }
}

void bot_tick(void)
{
  if (bot_priv.in_motion) {
    // generate current joint positions based on motion and tick state.
    int joints[8];
    trajectory_tick(bot_priv.motion, bot_priv.pos, joints);
    for (int i=0; i<8; i++) {
      servo_set_angle(i, joints[i]);
    }
    bot_priv.pos++;

    // if we're done with a motion, decrement counter and stop if we've
    // completed the requested number of iterations.
    if (trajectory_done(bot_priv.motion, bot_priv.pos)) {
      bot_priv.pos = 0;
      bot_priv.motion_count--;
      if (bot_priv.motion_count == 0) {
        bot_priv.in_motion = false;
      }
    }
  }
}

void bot_play_motion(trajectory_t motion, uint32_t amount)
{
  if (motion < TRAJ_MAX) {
    bot_priv.in_motion = true;
    bot_priv.motion = motion;
    bot_priv.motion_count = amount;
  }
}

bool bot_in_motion(void)
{
  return bot_priv.in_motion;
}

uint32_t bot_read_batt_mv(void)
{
  uint32_t sum = 0;
  const int count = 10;
  for (int i=0; i<count; i++) {
    uint32_t vbat_raw = adc_read(&hadc, ADC_CHANNEL_4);
    // /2 divider from vbat into 3.3v referenced 12b ADC
    sum += (vbat_raw*1000/620);
  }
  return sum / count;
}

void bot_set_eye(uint32_t lr, bool on)
{
  if (lr & RIGHT) {
    HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, (GPIO_PinState)on);
  }
  if (lr & LEFT) {
    HAL_GPIO_WritePin(LED2_GPIO_Port, LED2_Pin, (GPIO_PinState)on);
  }
}

uint32_t bot_time(void)
{
  return HAL_GetTick();
}

void bot_beep(uint32_t freq, uint32_t time, uint8_t volume)
{
  // frequency = clk / (period * prescaler)
  uint32_t psc = 187500/freq;
  htim1.Instance->PSC = psc;
  htim1.Instance->CCR1 = volume/2;
  htim1.Instance->CNT = 0;

  //HAL_GPIO_WritePin(SND_EN_GPIO_Port, SND_EN_Pin, (GPIO_PinState)1);
  HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_1);
  HAL_Delay(time);
  HAL_TIM_PWM_Stop(&htim1, TIM_CHANNEL_1);
  //HAL_GPIO_WritePin(SND_EN_GPIO_Port, SND_EN_Pin, (GPIO_PinState)0);
}
