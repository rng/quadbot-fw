#include "common.h"
#include "trajectory.h"

/*
Simple gaits for 8-DOF quadruped. Crawl gait for forward and turns.

Each gait is made up of two functions, each taking a tick (effectively gait
state) and leg index, and returning a joint angle:

  f_knee(t, leg) -> knee_angle
  f_shoulder(t, leg) -> shoulder_angle

*/

#define RT 24 // ramp time for knee joints (raise/lower time)

static int t_up(int t, int t0, int t1) {
  int dt = t1 - t0;
  return (256*(t-t0)) / dt;
}

static int t_dn(int t, int t0, int t1) {
  int dt = t1 - t0;
  return 256 - (256*(t-t0)) / dt;
}

static int t_scaleofs(int v, int scale, int ofs) {
  ASSERT((v >= 0) && (v <= 256));
  return (scale*v)/256 - ofs;
}

static int t_in(int v, int min, int max) {
  return (v >= min) && (v < max);
}

// Leg trajectory: forward

static int _fwd_knee(int t)
{
    int p = 0;
    //
    if      (t < RT)     p = t_up(t, 0,      RT-1);
    else if (t < 256-RT) p = 256;
    else if (t < 256)    p = t_dn(t, 256-RT, 255);
    //
    return t_scaleofs(p, -50, 0);
}

int _fwd_shoulder(int t)
{
    int p = 0;
    //
    if      (t < RT)     p = 0;
    else if (t < 256)    p = t_up(t, RT,     255);
    else if (t < 256+RT) p = 256;
    else                 p = t_dn(t, 256+RT, 1023);
    //
    const int da = 200;
    return t_scaleofs(p, da, da/2);
}

void traj_fwd(int p, int leg, int *kpos, int *spos)
{
  const int end = 1024;
  p = (p + end - leg*256) % end;
  *kpos = _fwd_knee(p);
  *spos = _fwd_shoulder(p);
}

// Leg trajectory: back

static int _back_knee(int t)
{
    int p = 0;
    //
    if      (t < RT)     p = t_up(t, 0,      RT-1);
    else if (t < 256-RT) p = 256;
    else if (t < 256)    p = t_dn(t, 256-RT, 255);
    //
    return t_scaleofs(p, -50, 0);
}

int _back_shoulder(int t)
{
    int p = 0;
    //
    if      (t < RT)     p = 0;
    else if (t < 256)    p = t_up(t, RT,     255);
    else if (t < 256+RT) p = 256;
    else                 p = t_dn(t, 256+RT, 1023);
    //
    const int da = -200;
    return t_scaleofs(p, da, da/2);
}

void traj_back(int p, int leg, int *kpos, int *spos)
{
  const int end = 1024;
  p = (p + end - leg*256) % end;
  *kpos = _back_knee(p);
  *spos = _back_shoulder(p);
}

// Leg trajectory: right turn

int _right_knee(int p, int leg)
{
  const int legmin = leg*256;
  const int legmax = (leg+1)*256;
  
  int up = 0;
  if (t_in(p, legmin, legmax)) {
    //
    if      (p < (legmin+RT)) up = t_up(p, legmin, legmin+RT);
    else if (p > (legmax-RT)) up = t_dn(p, legmax-RT, legmax);
    else                      up = 256;
  }
  //
  return t_scaleofs(up, -50, 0);
}

int _right_shoulder(int p, int leg)
{
  const int legmin = leg*256;
  const int legmax = (leg+1)*256;
  
  int ang = 0;

  if (t_in(p, legmin, legmax)) {
    ang = (256*(p-legmin)/256);
  } else if (p < 1024) {
    if (p >= (leg*256)) {
      ang = 256;
    }
  } else {
    ang = (256 - (256*(p-1024)/256));
  }
  //
  const int signs[] = {-1,-1,1,1};
  int sign = signs[leg];
  return t_scaleofs(ang, sign*120, 0);
}

void traj_right(int p, int leg, int *kpos, int *spos) {
  p = p % 1280;
  *kpos = _right_knee(p, leg);
  *spos = _right_shoulder(p, leg);
}

// Leg trajectory: left turn

int _left_knee(int p, int leg)
{
  const int legmin = leg*256;
  const int legmax = (leg+1)*256;
  
  int up = 0;
  if (t_in(p, legmin, legmax)) {
    //
    if      (p < (legmin+RT)) up = t_up(p, legmin, legmin+RT);
    else if (p > (legmax-RT)) up = t_dn(p, legmax-RT, legmax);
    else                      up = 256;
  }
  //
  return t_scaleofs(up, -50, 0);
}

int _left_shoulder(int p, int leg)
{
  const int legmin = leg*256;
  const int legmax = (leg+1)*256;
  
  int ang = 0;

  if (t_in(p, legmin, legmax)) {
    ang = (256*(p-legmin)/256);
  } else if (p < 1024) {
    if (p >= (leg*256)) {
      ang = 256;
    }
  } else {
    ang = (256 - (256*(p-1024)/256));
  }
  //
  const int signs[] = {1,1,-1,-1};
  int sign = signs[leg];
  return t_scaleofs(ang, sign*120, 0);
}

void traj_left(int p, int leg, int *kpos, int *spos) {
  p = p % 1280;
  *kpos = _left_knee(p, leg);
  *spos = _left_shoulder(p, leg);
}

// Leg trajectory: nod

int _nod_knee(int p, int leg)
{
  int up = 0;
  //
  if   (p < 128) up = t_up(p, 0,   128);
  else           up = t_dn(p, 128, 256);
  //
  const int signs[] = {1,-1,1,-1};
  int sign = signs[leg];
  return t_scaleofs(up, sign*90, 0);
}

int _nod_shoulder(int p, int leg)
{
  return 0;
}

void traj_nod(int p, int leg, int *kpos, int *spos) {
  p = p % 256;
  *kpos = _nod_knee(p, leg);
  *spos = _nod_shoulder(p, leg);
}

// Handler

typedef void (*traj_func_t)(int, int, int*, int*);

typedef struct {
  traj_func_t func;
  int size;
} traj_handler_t;

static const traj_handler_t handlers[] = {
  { traj_fwd,   1024 },
  { traj_right, 1280 },
  { traj_back,  1024 },
  { traj_left,  1280 },
  { traj_nod,   256 },
};

void trajectory_tick(trajectory_t traj, int pos, int* joints)
{
  traj_func_t func = handlers[traj].func;
  func(pos, 0, &joints[0], &joints[1]); // br
  func(pos, 1, &joints[2], &joints[3]); // fr
  func(pos, 2, &joints[7], &joints[6]); // bl
  func(pos, 3, &joints[4], &joints[5]); // fl
}

bool trajectory_done(trajectory_t traj, int pos) {
  return (pos % handlers[traj].size) == 0;
}
