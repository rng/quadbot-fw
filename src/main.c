#include "stm32f0xx_hal.h"
#include "usb_device.h"
#include "usbd_cdc_if.h"
#include "hal.h"
#include "ps2ctrl.h"
#include "common.h"
#include "bot.h"
#include "vm.h"

typedef struct {
  vm_t vm;
} state_priv_t;

state_priv_t state_priv;

enum {
  DBG_CMD_VM_READ_RSTACK = 0,
  DBG_CMD_VM_READ_HEAP,
  DBG_CMD_VM_LOAD,
  DBG_CMD_VM_RUN,
  DBG_CMD_VM_STOP,
  DBG_CMD_VM_STATE,
  DBG_CMD_VM_RESULT,
  DBG_CMD_VM_EXCEPTION,
  DBG_CMD_VM_RESET,
};

void dbg_handle_packet(uint8_t *buf, size_t len)
{
  uint32_t val;
  uint8_t b;
  uint8_t cmd = buf[0];

  switch(cmd) {
    // Read a value from the return stack
    case DBG_CMD_VM_READ_RSTACK:
      if (len != 2) goto fail;
      val = vm_get_rstack(&state_priv.vm, buf[1]);
      CDC_Transmit_FS((uint8_t*)&val, sizeof(val));
      break;

    // Read a value from the heap
    case DBG_CMD_VM_READ_HEAP:
      if (len != 3) goto fail;
      val = (buf[1]<<8) | buf[2];
      val = mem_read_raw(val);
      CDC_Transmit_FS((uint8_t*)&val, sizeof(val));
      break;

    // Write bytecode into the VM code buffer
    case DBG_CMD_VM_LOAD:
      if (len < 4) goto fail;
      vm_stop(&state_priv.vm);
      memcpy(state_priv.vm.code+buf[1], buf+3, buf[2]);
      break;

    // Start executing at a given address
    case DBG_CMD_VM_RUN:
      if (len != 2) goto fail;
      vm_run(&state_priv.vm, buf[1]);
      break;

    // Stop executing
    case DBG_CMD_VM_STOP:
      if (len != 1) goto fail;
      vm_stop(&state_priv.vm);
      break;

    // Return current VM state (running or not)
    case DBG_CMD_VM_STATE:
      if (len != 1) goto fail;
      b = vm_is_running(&state_priv.vm);
      CDC_Transmit_FS(&b, sizeof(b));
      break;

    // Pop and return the top of the data stack if present
    case DBG_CMD_VM_RESULT:
      if (len != 1) goto fail;
      val = vm_get_result(&state_priv.vm);
      CDC_Transmit_FS((uint8_t*)&val, sizeof(val));
      break;

    // Return the current VM exception
    case DBG_CMD_VM_EXCEPTION:
      if (len != 1) goto fail;
      b = vm_exception(&state_priv.vm);
      CDC_Transmit_FS(&b, sizeof(b));
      break;

    // Reset VM state
    case DBG_CMD_VM_RESET:
      if (len != 1) goto fail;
      vm_reset(&state_priv.vm);
      break;

    default:
      goto fail;
  }
  return;

fail:
  abort();
}

static bool usb_powered(void)
{
  return HAL_GPIO_ReadPin(VUSB_GPIO_Port, VUSB_Pin);
}

static void init(void)
{
  HAL_Init();
  SystemClock_Config();
  MX_GPIO_Init();
  MX_ADC_Init();
  //MX_USART1_UART_Init();
  MX_USB_DEVICE_Init();
  MX_TIM1_Init();
  MX_TIM2_Init();
  MX_TIM3_Init();
  MX_SPI1_Init();
  // Stop servos if we're stopped via the debugger
  DBGMCU->APB2FZ |= DBGMCU_APB2_FZ_DBG_TIM1_STOP;
  DBGMCU->APB1FZ |= DBGMCU_APB1_FZ_DBG_TIM2_STOP;
  DBGMCU->APB1FZ |= DBGMCU_APB1_FZ_DBG_TIM3_STOP;
  // wait until we're sufficiently charged
  while (usb_powered() && (bot_read_batt_mv() < 3800)) {
    bot_set_eye(LEFT|RIGHT,  1);
    HAL_Delay(200);
    bot_set_eye(LEFT|RIGHT,  0);
    HAL_Delay(1000);
  }

  bot_init();
  vm_init(&state_priv.vm);
  ps2ctrl_init(true);
}

static void controller_tick(void)
{
  ps2ctrl_state_t ctrl;
  trajectory_t move = TRAJ_MAX;
  int eyes = 0;

  // handle motion from controller
  if (ps2ctrl_read(&ctrl)) {
    // motions in order of ascending precedence
    if (ctrl.buttons & PS2CTRL_BTN_UP)    move = TRAJ_FD;
    if (ctrl.buttons & PS2CTRL_BTN_LEFT)  move = TRAJ_LT;
    if (ctrl.buttons & PS2CTRL_BTN_RIGHT) move = TRAJ_RT;
    if (ctrl.buttons & PS2CTRL_BTN_TRI)   move = TRAJ_NOD;
    // eyes
    if (ctrl.buttons & PS2CTRL_BTN_L1)    eyes |= LEFT;
    if (ctrl.buttons & PS2CTRL_BTN_R1)    eyes |= RIGHT;

    // set eyes
    bot_set_eye(LEFT|RIGHT, 0);
    bot_set_eye(eyes, 1);
    // set motion if appropriate
    if ((!bot_in_motion()) && (move != TRAJ_MAX)) {
      bot_play_motion(move, 1);
    }
  }
}

int main(void)
{
  init();

  int tick = 0;
  while (1) {
    // perform gait trajectory tick and update joints
    bot_tick();
    // check controller every ~100ms
    if ((tick % 50) == 0) {
      controller_tick();
    }
    // update VM until ready for another motion update
    const int delay_ms = 2;
    uint32_t tickstart = HAL_GetTick();
    while((HAL_GetTick() - tickstart) < delay_ms) {
      vm_interp(&state_priv.vm);
    }
    tick++;
  }
}

void assert_failed(uint8_t* file, uint32_t line)
{
  abort();
}

void Error_Handler(void)
{
  abort();
}

void abort(void) {
    while (1) {};
}

void _init(void) {
}
