#!/usr/bin/env python

import serial
import struct
import sys
from PySide import QtCore, QtGui


EN, POS = range(2)
BRK, BRS, FRK, FRS, FLK, FLS, BLS, BLK = range(8)


def cmdb(port, cmd, idx, val):
  s = struct.pack("bbbb", cmd, idx, val, 0)
  port.write(s)

def cmdh(port, cmd, idx, val):
  s = struct.pack("bbh", cmd, idx, val)
  port.write(s)

ROWS = "BR FR FL BL".split()
INDICES = [0,1,2,3,4,5,7,6]
ZEROINIT = [-70, -6, -35, 11, -6, -2, -11, 15]


class sliderdemo(QtGui.QWidget):
  def __init__(self, parent = None):
    super(sliderdemo, self).__init__(parent)

    layout = QtGui.QGridLayout()

    self.oldpos = [0]*8
    self.en = [False]*8
    self.sl = []
    self.zeros = ZEROINIT
    self.p = serial.Serial("/dev/ttyACM0", 115200)
    for i in range(8):
      cmdb(self.p, EN, i, 0)


    lbl = QtGui.QLabel("Knee")
    layout.addWidget(lbl, 0, 1)
    lbl = QtGui.QLabel("Shoulder")
    layout.addWidget(lbl, 0, 2)

    for row in range(4):
      lbl = QtGui.QLabel(ROWS[row])
      layout.addWidget(lbl, row+1, 0)
      for col in range(2):
        sl = QtGui.QSlider(QtCore.Qt.Horizontal)
        sl.setMinimum(-150)
        sl.setMaximum(150)
        sl.setValue(0)
        sl.setTickPosition(QtGui.QSlider.TicksBelow)
        sl.setTickInterval(5)
      
        layout.addWidget(sl, row+1, col+1)
        sl.valueChanged.connect(self.valuechange)
        self.sl.append(sl)
    
    but_zero = QtGui.QPushButton("zero")
    but_zero.clicked.connect(self.zero)
    layout.addWidget(but_zero, 5, 0)
  
    self.setLayout(layout)
    self.setWindowTitle("SpinBox demo")

    self.timer = QtCore.QTimer()
    self.timer.timeout.connect(self.tick)
    self.dt = 10
    self.timer.start(self.dt)
    self.t = 0

  def gait(self, t):
    t = int(t%1024)
    up = 0
    if t < 13:
      up = (256*t/13)
    elif 13 <= t <= 243:
      up = 256
    elif t < 256:
      up = 256-(256*(t-243)/13)

    ang = 0
    if t < 256:
      fwd = (256*t/256)
    else:
      x = 256*(t-256)/768
      fwd = (256-x)

    assert 0 <= up <= 256
    assert 0 <= fwd <= 256

    da = 200
    ang = (fwd*da)/256
    return -int((90*up)/256), int(ang-da/2.)

  def tick(self):
    def l(ofs, knee, shoulder):
      up, angle = self.gait(t-ofs*256)
      self.setpos(knee, up)
      self.setpos(shoulder, angle)

    self.t += self.dt/1000.

    t = self.t*50
  
    l(0, 0,1) #br
    l(1, 2,3) #fr
    l(2, 6,7) #bl
    l(3, 4,5) #fl

  def zero(self):
    for i in range(8):
       pos = self.sl[i].value()
       print "%d=%d" % (i, pos)

  def setpos(self, i, pos):
    if self.sl[i].value() != pos:
      self.sl[i].setValue(pos)
    if pos != self.oldpos[i]:
      self.oldpos[i] = pos
      idx = INDICES[i]
      if not self.en[i]:
        self.en[i] = True
        cmdb(self.p, EN, idx, 1)
      cmdh(self.p, POS, idx, pos + self.zeros[i])

  def valuechange(self):
    for i in range(8):
       pos = self.sl[i].value()
       if pos != self.oldpos[i]:
         #print "%d changed to %d" % (i, pos)
         self.oldpos[i] = pos
         idx = INDICES[i]
         if not self.en[i]:
           self.en[i] = True
           cmdb(self.p, EN, idx, 1)
         cmdh(self.p, POS, idx, pos + self.zeros[i])
		
def main():
  app = QtGui.QApplication(sys.argv)
  ex = sliderdemo()
  ex.show()
  sys.exit(app.exec_())
	

if __name__ == '__main__':
   main()
