#pragma once

#include <stdbool.h>

void servo_set_pos(int n, int pos);
void servo_set_angle(int n, int angle);
void servo_lerp(int n, int end_angle, int ms);
void servo_enable(int n, bool on);
