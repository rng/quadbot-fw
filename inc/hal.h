#pragma once

void SystemClock_Config(void);
void MX_ADC_Init(void);
void MX_TIM1_Init(void);
void MX_TIM2_Init(void);
void MX_TIM3_Init(void);
void MX_USART1_UART_Init(void);
void MX_GPIO_Init(void);
void MX_SPI1_Init(void);
