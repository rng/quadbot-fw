#pragma once

#include <stdint.h>
#include <stddef.h>

// Tagged value type
typedef uint16_t mem_val_t;

// Value tags
typedef enum {
  MEM_TAG_INT  = 0,
  MEM_TAG_BLK  = 1,
  MEM_TAG_PAIR = 2,
  // gc mark bit
  MEM_TAG_MARK = 4,
} mem_tag_t;

// Tagged values (creation + getting tag & value)
#define MK_TAGGED(val, tag) (((val)<<3)|(tag))
#define TAGGED_VAL(tagged) (tagged >> 3)
#define TAGGED_TAG(tagged) (tagged & 3)

// Null type (pointer to 0'th cell)
#define NIL MK_TAGGED(0, MEM_TAG_PAIR)

typedef void (*mem_root_visit_cb_t)(void*);

// Init heap
void mem_init(uint8_t *heap, size_t heap_size,
              mem_root_visit_cb_t root_visit, void *root_ctx);
// Called by client code on GC to mark roots
void mem_mark(mem_val_t val);
uint32_t mem_read_raw(mem_val_t obj);
// Cons cell (pair) creation and accessors
mem_val_t mem_pair(mem_val_t car, mem_val_t cdr);
mem_val_t mem_car(mem_val_t pair);
mem_val_t mem_cdr(mem_val_t pair);
void mem_set_car(mem_val_t pair, mem_val_t val);
void mem_set_cdr(mem_val_t pair, mem_val_t val);
