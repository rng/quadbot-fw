#pragma once

#include <stdint.h>
#include <stdbool.h>
#include "trajectory.h"

enum {
  LEFT = 1,
  RIGHT = 2,
};

void bot_init(void);
void bot_tick(void);
void bot_play_motion(trajectory_t motion, uint32_t amount);
bool bot_in_motion(void);
uint32_t bot_read_batt_mv(void);
void bot_set_eye(uint32_t lr, bool on);
uint32_t bot_adc_read(uint32_t channel);
uint32_t bot_time(void);
void bot_beep(uint32_t freq, uint32_t time, uint8_t volume);
