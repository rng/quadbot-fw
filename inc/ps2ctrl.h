#pragma once

/*
Single-file platform-agnostic Playstation 2 controller library.

Based on docs at: http://store.curiousinventor.com/guides/PS2/
*/

#include <stdint.h>
#include <stdbool.h>
#include <string.h>

// Interface ------------------------------------------------------------------

enum {
  PS2CTRL_BTN_SELECT = (1<<0),
  PS2CTRL_BTN_L3     = (1<<1),
  PS2CTRL_BTN_R3     = (1<<2),
  PS2CTRL_BTN_START  = (1<<3),
  PS2CTRL_BTN_UP     = (1<<4),
  PS2CTRL_BTN_RIGHT  = (1<<5),
  PS2CTRL_BTN_DOWN   = (1<<6),
  PS2CTRL_BTN_LEFT   = (1<<7),
  PS2CTRL_BTN_L2     = (1<<8),
  PS2CTRL_BTN_R2     = (1<<9),
  PS2CTRL_BTN_L1     = (1<<10),
  PS2CTRL_BTN_R1     = (1<<11),
  PS2CTRL_BTN_TRI    = (1<<12),
  PS2CTRL_BTN_CIRCLE = (1<<13),
  PS2CTRL_BTN_CROSS  = (1<<14),
  PS2CTRL_BTN_SQUARE = (1<<15),
} ps2ctrl_btn_t;

enum {
  PS2CTRL_PRS_RIGHT  = 0,
  PS2CTRL_PRS_LEFT   = 1,
  PS2CTRL_PRS_UP     = 2,
  PS2CTRL_PRS_DOWN   = 3,
  PS2CTRL_PRS_TRI    = 4,
  PS2CTRL_PRS_CIRCLE = 5,
  PS2CTRL_PRS_CROSS  = 6,
  PS2CTRL_PRS_SQUARE = 7,
  PS2CTRL_PRS_L1     = 8,
  PS2CTRL_PRS_R1     = 9,
  PS2CTRL_PRS_L2     = 10,
  PS2CTRL_PRS_R2     = 11,
} ps2ctrl_prs_t;

typedef struct {
  int8_t x;
  int8_t y;
} ps2ctrl_joy_t;

typedef struct {
  uint16_t buttons;
  ps2ctrl_joy_t joyl;
  ps2ctrl_joy_t joyr;
  uint8_t pressures[12];
} ps2ctrl_state_t;

// Implemented by client - SPI transfer a buffer to/from the controller.
void ps2ctrl_sendrecv(uint8_t *tbuf, uint8_t *rbuf, size_t buf_len);

// Public API

// Initialise controller
bool ps2ctrl_init(bool analog);

// Poll controller and populate state structure
bool ps2ctrl_read(ps2ctrl_state_t *state);

// Misc
void ps2ctrl_read_model(void);
void ps2ctrl_enable_analog(void);
void ps2ctrl_enter_config(void);
void ps2ctrl_leave_config(void);


// Implementation -------------------------------------------------------------

#ifdef PS2CTRL_IMPL

enum {
  PS2CTRL_CMD_QUERY_ANALOG   = 0x41,
  PS2CTRL_CMD_READ_DATA      = 0x42,
  PS2CTRL_CMD_CONFIG         = 0x43,
  PS2CTRL_CMD_MODE_AND_LOCK  = 0x44,
  PS2CTRL_CMD_QUERY_MODEL    = 0x45,
  PS2CTRL_CMD_VIBRATION      = 0x4d,
};

static bool ps2ctrl_parse_read(uint8_t *buf, ps2ctrl_state_t *state)
{
  bool buttons = false;
  bool joys = false;
  bool pressures = false;
  // check what we expect
  uint8_t mode = buf[1];
  if (mode == 0x41) {
    buttons = true;
  } else if (mode == 0x73) {
    buttons = joys = true;
  } else if (mode == 0x79) {
    buttons = joys = pressures = true;
  } else {
    return false;
  }
  memset(state, 0, sizeof(ps2ctrl_state_t));
  // parse output
  if (buttons) {
    state->buttons = ~((buf[4]<<8) | buf[3]);
  }
  if (joys) { 
    state->joyr.x = buf[5];
    state->joyr.y = buf[6];
    state->joyl.x = buf[7];
    state->joyl.y = buf[8];
  }

  if (pressures) {
    memcpy(state->pressures, &buf[9], 12);
  }
  return true;
}

void ps2ctrl_enter_config(void)
{
  static const uint8_t cmd[] = {0x01, PS2CTRL_CMD_CONFIG, 0x00,
                                0x01, 0x00};
  uint8_t resp[5] = {0};
  ps2ctrl_sendrecv((uint8_t*)cmd, resp, sizeof(cmd));
}

void ps2ctrl_leave_config(void)
{
  static const uint8_t cmd[] = {0x01, PS2CTRL_CMD_CONFIG, 0x00,
                                0x00, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a};
  uint8_t resp[9] = {0};
  ps2ctrl_sendrecv((uint8_t*)cmd, resp, sizeof(cmd));
}

void ps2ctrl_enable_analog(void)
{
  static const uint8_t cmd[] = {0x01, PS2CTRL_CMD_MODE_AND_LOCK, 0x00,
                                0x01, 0x03, 0x00, 0x00, 0x00, 0x00};
  uint8_t resp[9] = {0};
  ps2ctrl_sendrecv((uint8_t*)cmd, resp, sizeof(cmd));
}

void ps2ctrl_read_model(void)
{
  static const uint8_t cmd[] = {0x01, PS2CTRL_CMD_QUERY_MODEL, 0x00,
                                0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a};
  uint8_t resp[9] = {0};
  ps2ctrl_sendrecv((uint8_t*)cmd, resp, sizeof(cmd));
}

bool ps2ctrl_read(ps2ctrl_state_t *state)
{
  uint8_t cmd[21] = {0};
  uint8_t resp[21] = {0};
  cmd[0] = 0x01;
  cmd[1] = PS2CTRL_CMD_READ_DATA;
  ps2ctrl_sendrecv(cmd, resp, 9);
  return ps2ctrl_parse_read(resp, state); 
}

bool ps2ctrl_init(bool analog)
{
  ps2ctrl_enter_config();
  if (analog) {
    ps2ctrl_enable_analog();
  }
  ps2ctrl_leave_config();
  return true;
}

#endif
