#pragma once

#define LED1_Pin GPIO_PIN_1
#define LED1_GPIO_Port GPIOF
#define ADC_EYE0_Pin GPIO_PIN_0
#define ADC_EYE0_GPIO_Port GPIOA
#define ADC_EYE1_Pin GPIO_PIN_1
#define ADC_EYE1_GPIO_Port GPIOA
#define ADC_BATT_Pin GPIO_PIN_4
#define ADC_BATT_GPIO_Port GPIOA
#define VUSB_Pin GPIO_PIN_10
#define VUSB_GPIO_Port GPIOA
#define SPI_CS_Pin GPIO_PIN_8
#define SPI_CS_GPIO_Port GPIOB
#define LED2_Pin GPIO_PIN_7
#define LED2_GPIO_Port GPIOB
#define SND_EN_Pin GPIO_PIN_6
#define SND_EN_GPIO_Port GPIOB
