#pragma once

#include <stdbool.h>

typedef enum {
  TRAJ_FD  = 0,
  TRAJ_RT  = 1,
  TRAJ_BK  = 2,
  TRAJ_LT  = 3,
  TRAJ_NOD = 4,
  TRAJ_MAX,
} trajectory_t;

void trajectory_tick(trajectory_t traj, int pos, int* joints);
bool trajectory_done(trajectory_t traj, int pos);
