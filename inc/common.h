#pragma once

#include <stdint.h>
#include <stdlib.h>

#define ASSERT(v) { \
  if (!(v)) abort(); \
}
