#pragma once

#include <stdint.h>
#include <stdbool.h>
#include <setjmp.h>
#include "mem.h"

/**
Stack VM for a small Logo-esque language.
 - 13bit tagged, stop the world GC, integers and lists
 - Dynamically scoped
 - Tail calls
 - 64 bytes of user visible program space
 - 192 byte of ROM standard lib
**/

enum {
  VM_MAX_ENV = 16,
  VM_MAX_CODE = 256,
  VM_MAX_USER_CODE = 64,
  VM_MAX_STACK = 16,
  VM_MAX_HEAP = 256,
};

// Data and return stacks
typedef struct {
  mem_val_t d[VM_MAX_STACK];
  uint8_t p;
} stack_t;

// Environment pair (variable id to value mapping)
typedef struct {
  mem_val_t id;
  mem_val_t value;
} env_pair_t;

// VM exceptions
typedef enum {
  VM_EXCEPT_NONE = 0,
  VM_EXCEPT_OVERFLOW,
  VM_EXCEPT_UNDERFLOW,
  VM_EXCEPT_UNKNOWN_VAR,
  VM_EXCEPT_UNKNOWN_PRIM,
  VM_EXCEPT_UNKNOWN_INST,
  VM_EXCEPT_TOO_MANY_VAR,
  VM_EXCEPT_INVALID_TYPE,
  VM_EXCEPT_LIST_BOUNDS,
  VM_EXCEPT_OUT_OF_MEM,
  VM_EXCEPT_DIV0,
} vm_exception_t;

// VM State
typedef struct {
  stack_t dstack;
  stack_t rstack;
  uint8_t code[VM_MAX_USER_CODE];
  env_pair_t env[VM_MAX_ENV];
  uint8_t heap[VM_MAX_HEAP];
  uint16_t pc;
  uint8_t env_top;
  bool running;
  vm_exception_t exc;
  uint32_t wait_until;
  jmp_buf except;
} vm_t;

void vm_init(vm_t *vm);
void vm_interp(vm_t *vm);
void vm_run(vm_t *vm, uint16_t start);
void vm_stop(vm_t *vm);
void vm_reset(vm_t *vm);
bool vm_is_running(vm_t *vm);
uint32_t vm_get_result(vm_t *vm);
uint32_t vm_get_rstack(vm_t *vm, size_t idx);
vm_exception_t vm_exception(vm_t *vm);
