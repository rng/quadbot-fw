#!/usr/bin/env python

# SLogo - a minimal, hacky (disgraceful to the Logo name!), Logo-esque language
# compiling to bytecode.
#
# Logo is a very dynamic language, so in order to simplify compiling down to
# something compact we take a few liberties (think of it like a dynamic scoped
# scheme with a Logo reader):
# - Blocks and lists are separate concepts [] vs {}
# - Blocks are parsed at compile time (not quoted), limiting their flexibility
# - Symbols are interned host-side and there is no runtime interning

import os
import time
import atexit
import readline
import argparse
from tool import dbgif
from collections import namedtuple


if os.path.exists(".replhist"):
    readline.read_history_file(".replhist")
atexit.register(readline.write_history_file, ".replhist")

Prim = namedtuple("Prim", "num arity")
Func = namedtuple("Func", "args arity")

OPS = "jf blk prim imm call tail ret def get".split()
BINOPS = "> < = >= <= <> - + / *".split()
PRIMS = [
  ("halt", 0), ("fd", 1), ("bk", 1), ("rt", 1), ("lt", 1),
  ("random", 1), ("joint", 2), ("adc", 1), ("led", 2), ("beep", 2),
  ("list", 2), ("item", 2), ("count", 1), ("apply", 1), ("wait", 1),
  ("motion", 1), ("pwr", 1), ("first", 1), ("rest", 1), ("fput", 2),
]


# A stellar lexer (totally a band name)
def lex(src):
  toks = []
  p = 0
  while p < len(src):
    if src[p] == ";":
      while p < len(src) and src[p] != "\n": p += 1
    elif src[p] in " \t\r\n": p += 1
    elif src[p] in "()[]{}+-*/": toks.append(src[p]); p += 1
    elif src[p] in "<>=":
      if src[p+1] in "<>=":
        p += 1
        toks.append(src[p-1]+src[p])
      else:
        toks.append(src[p])
      p += 1
    else:
      tok = ""
      while p < len(src) and src[p] not in "()[]{}+-*/<>= \n\r\t":
        tok += src[p]; p += 1
      toks.append(tok)
  return toks

# Parser
class ParseException(Exception): pass
class UnexpectedEOFException(ParseException): pass


def get_sym(syms, word):
  if word not in syms:
    syms[word] = len(syms)
  return syms[word]


def consume(toks):
  if len(toks) == 0:
    raise UnexpectedEOFException("Unexpected end of input")
  return toks.pop(0)


def peek(toks):
  if len(toks) == 0:
    raise  UnexpectedEOFException("Unexpected end of input")
  return toks[0]


def parse_to(toks, funcs, syms):
  consume(toks)
  name = consume(toks)
  args = []
  while peek(toks).startswith(":"):
    args.append(get_sym(syms, consume(toks)))
  body = []
  # predefine for recursive calls
  funcs[name] = Func(args, len(args))
  while len(toks) and peek(toks) != "end":
    body.append(parse_expr(toks, funcs, syms))
  consume(toks)
  return ("to", name, args, body)


def parse_prim(toks, funcs, syms):
  word = consume(toks)
  try:
    val = int(word)
    return ("imm", val)
  except:
    pass
  if word == "(":
    r = parse_expr(toks, funcs, syms)
    assert consume(toks) == ")"
    return r
  if word == "[":
    args = []
    while len(toks) and peek(toks) != "]":
      args.append(parse_expr(toks, funcs, syms))
    args.append(("imm", len(args)))
    consume(toks)
    return ("prim", funcs["list"].num, args)
  if word.startswith(":"):
    return ("var", get_sym(syms, word))
  if word in funcs:
    func = funcs[word]
    args = [parse_expr(toks, funcs, syms) for _ in range(func.arity)]
    if isinstance(func, Prim):
      return ("prim", func.num, args)
    return ("call", word, args)
  if word in ["{", "}", "(", ")", "[", "]", "end"]:
    raise ParseException("Unexpected '%s'" % word)
  raise ParseException("Unknown word '%s'" % word)


def parse_expr(toks, funcs, syms):
  if peek(toks) == "{":
    consume(toks)
    block = ["block"]
    while len(toks) and peek(toks) != "}":
      block.append(parse_expr(toks, funcs, syms))
    consume(toks)
    return tuple(block)
  elif peek(toks) == "if":
    consume(toks)
    te = parse_expr(toks, funcs, syms)
    tb = parse_expr(toks, funcs, syms)
    return ("if", te, tb, None)
  elif peek(toks) == "ifelse":
    consume(toks)
    te = parse_expr(toks, funcs, syms)
    tb = parse_expr(toks, funcs, syms)
    fb = parse_expr(toks, funcs, syms)
    return ("if", te, tb, fb)
  elif peek(toks) == "make":
    consume(toks)
    name = get_sym(syms, ":"+consume(toks))
    value = parse_prim(toks, funcs, syms)
    return ("make", name, value)
  else:
      p = parse_prim(toks, funcs, syms)
      while len(toks) and peek(toks) in BINOPS:
        op = consume(toks)
        r = parse_prim(toks, funcs, syms)
        p = ("binop", BINOPS.index(op), p, r)
      return p


def parse(toks, funcs, syms):
  r = []
  while len(toks):
    if peek(toks) == "to":
      r.append(parse_to(toks, funcs, syms))
    else:
      r.append(parse_expr(toks, funcs, syms))
  return r


# Codegen
def flatten(ast, func_addrs, pc2src, func_start=0, haltafter=False):
  r = []
  def emit1(op, arg=0):
    assert op in OPS, ("unhandled op %s" % op)
    if 0 <= arg < 15:
      r.append((OPS.index(op)<<4) | arg)
      return len(r)-1
    else:
      r.append((OPS.index(op)<<4) | 15)
      r.append(arg)
      return len(r)-2
  
  def _patch(addr):
    op,arg = r[addr]>>4, r[addr]&0xf
    assert (op in [0, 1]) and (arg == 0), "invalid jump %d at %d" % (op, addr)
    ofs = (len(r) - addr - 1)
    if 0 <= ofs < 15:
      r[addr] = (op<<4) | ofs
      return 0
    else:
      r[addr] = (op<<4) | 15
      r.insert(addr+1, ofs)
      return 1

  def _last(i, body): return i == len(body)-1
  def _savepc(func, expr): pc2src[len(r) + func_start] = (func, expr)

  def _visit(expr, func, tail=False):
    if isinstance(expr, tuple):
      if expr[0] == "to":
        func_addrs[expr[1]] = len(r) + func_start
        for a in reversed(expr[2]):
          emit1("def", a)
        for e in expr[3]:
          _visit(e, expr[1])
        emit1("ret")
      elif expr[0] == "make":
        _visit(expr[2], func)
        emit1("def", expr[1])
      elif expr[0] == "prim":
        for a in expr[2]:
          _visit(a, func)
        emit1("prim", len(BINOPS)+expr[1]) # binops are first primitives
        _savepc(func, expr)
      elif expr[0] == "call":
        for a in expr[2]:
          _visit(a, func)
        emit1("tail" if tail else "call", func_addrs[expr[1]])
        _savepc(func, expr)
      elif expr[0] == "if":
        _visit(expr[1], func)
        p = emit1("jf", 0)
        assert expr[2][0] == "block"
        for i, a in enumerate(expr[2][1:]):
          _visit(a, func, tail=_last(i, expr[2][1:]))
        if expr[3] is not None:
          emit1("imm", 0)
          q = emit1("jf", 0)
          q += _patch(p)
          assert expr[3][0] == "block"
          for i, a in enumerate(expr[3][1:]):
            _visit(a, func, tail=_last(i, expr[3][1:]))
          _patch(q)
        else:
          _patch(p)
      elif expr[0] == "binop":
        _visit(expr[2], func)
        _visit(expr[3], func)
        emit1("prim", expr[1])
        _savepc(func, expr)
      elif expr[0] == "var":
        emit1("get", expr[1])
      elif expr[0] == "block":
        p = emit1("blk", 0)
        for i, e in enumerate(expr[1:]):
          _visit(e, func, tail=_last(i, expr[1:]))
        emit1("ret")
        _patch(p)
      elif expr[0] == "imm":
        emit1("imm", expr[1])
      else:
        raise Exception("unhandled '%s'" % str(expr))
    else:
      raise Exception("unhandled '%s'" % str(expr))

  for a in ast:
    _visit(a, "repl")

  if len(ast) and haltafter:
    emit1("prim", len(BINOPS)) # halt prim

  return r


def disasm(asm, pcofs=0):
  pc = 0
  while pc < len(asm):
    opc = pc
    ins = asm[pc]; pc += 1
    op, arg = ins >> 4, ins & 0xf
    if arg == 15: arg = asm[pc]; pc += 1
    if op in [0,1]: arg += (pc + pcofs)
    if op == 2: arg = (BINOPS+list(zip(*PRIMS)[0]))[arg]
    hexd = " ".join("%02x" % v for v in asm[opc:pc])
    print("%3d: %-6s %-4s %-4s" % (opc+pcofs, hexd, OPS[op], arg))


# Compiler state
class CompState(object):
  def __init__(self, interface, verbose=False):
    # build function table (with primitives initially)
    def _prim(i, name, arity):
      return (name, Prim(i, arity))
    self.fns = dict((_prim(i, n, a) for i, (n, a) in enumerate(PRIMS)))
    # 
    self.addrs = {}
    self.syms = {}
    self.pc2src = {}
    self.code_size = 0
    self.dbg = dbgif.debugif(interface)
    self.verbose = verbose


def comp(src, state):
  toks = lex(src)
  # parse split into function defs vs exprs
  ast = parse(toks, state.fns, state.syms)
  funcdefs = [a for a in ast if a[0] == "to"]
  exprs = [a for a in ast if a[0] != "to"]
  # codegen for the defs and exprs
  func_asm = flatten(funcdefs, state.addrs, state.pc2src, 
                     func_start=state.code_size)
  expr_asm = flatten(exprs, state.addrs, state.pc2src, haltafter=True)
  return func_asm, expr_asm


def backtrace(state):
  # fetch pc, env pairs from VM
  stk = []
  i = 0
  while 1:
    pc = state.dbg.rstack_read(i)
    if pc is None: break
    env = state.dbg.rstack_read(i+1)
    stk.append((pc, env))
    i += 2
  # return the source expression for a given bytecode PC
  def _getsrc(pc):
    if pc-1 in state.pc2src:
      return state.pc2src[pc-1]
    if pc in state.pc2src:
      return state.pc2src[pc]
    return ["?", "?"]
  # "unparse" an AST expression for pretty-printing
  def _pp(exp):
    if len(exp) == 0: return "?"
    if exp[0] == "binop":
      return "%s %s %s" % (_pp(exp[2]), BINOPS[exp[1]], _pp(exp[3]))
    elif exp[0] == "prim":
      return "%s %s" % (PRIMS[exp[1]][0], " ".join(map(_pp, exp[2])))
    elif exp[0] == "call":
      return "%s %s" % (exp[1], " ".join(map(_pp, exp[2])))
    elif exp[0] == "block":
      return "{ %s }" % (" ".join(map(_pp, exp[1:])))
    elif exp[0] == "imm":
      return "%d" % exp[1]
    elif exp[0] == "var":
      return "%s" % {v:k for (k,v) in state.syms.items()}[exp[1]]
    else:
      return str(exp)
  # print the actual backtrace
  print "Backtrace:"
  for pc, _ in stk:
    func, expr = _getsrc(pc)
    print " %6s | %s" % (func, _pp(expr))


def run(src, state):
  func_asm, expr_asm = comp(src, state)
  # load functions, incrementing code pointer
  if len(func_asm):
    if state.verbose: disasm(func_asm)
    state.dbg.load(func_asm, state.code_size)
    state.code_size += len(func_asm)
  # load and run expressions
  if len(expr_asm):
    if state.verbose: disasm(expr_asm)
    state.dbg.load(expr_asm, state.code_size)
    state.dbg.run(state.code_size)
    # wait for execution to finish
    while state.dbg.running():
      try:
        time.sleep(0.1)
      except KeyboardInterrupt:
        state.dbg.stop()
    # check if we hit an exception
    exc = state.dbg.exception()
    if exc != 0:
      print "Runtime exception:", exc
      backtrace(state)
    else:
      # otherwise print result
      res = state.dbg.result()
      if res is not None:
        print res


def load(fn, state):
  run(open(fn).read(), state)


# Compile the stdlib into a header to include in the firmware
def stdlib(fn, state):
  state.code_size = 64
  func_asm, expr_asm = comp(open(fn).read(), state)
  state.code_size = 0
  assert len(expr_asm) == 0
  assert len(func_asm) <= 192
  if state.verbose: disasm(func_asm, pcofs=64)
  rom_code = ",".join("0x%02x" % v for v in func_asm)
  with open("inc/slogo_rom.h", "w") as f:
    f.write("#pragma once\n\n")
    f.write("// ROM bytecode compiled from %s\n" % fn)
    f.write("static const uint8_t slogo_rom[] = {%s};\n" % rom_code);


def repl(args, state):
  print "Slogo REPL. Connecting via %s. Ctrl-D to quit" % args.interface
  state.dbg.reset()
  expr, needmore = "", False
  while 1:
    # try to read source from prompt, appending to previous partial if needed
    try:
      expr += raw_input(".. " if needmore else ">> ") + "\n"
    except KeyboardInterrupt:
      print
      expr, needmore = "", False
    # attempt to compile/load/run
    try:
      run(expr, state)
    except UnexpectedEOFException as e:
      needmore = True # unexpected end of input on parse, wait for more input
      continue
    except Exception as e:
      print "! Compile error:", e # other failure
    # reset prompt state
    expr, needmore = "", False


def main():
  parser = argparse.ArgumentParser(description="Slogo compiler/REPL")
  parser.add_argument("-i", dest="interface", choices=["usb"],
                      help="interface to target", default="usb")
  parser.add_argument("-v", dest="verbose", action="store_true",
                      help="verbose mode")
  parser.add_argument("sources", metavar="SRC", nargs="*",
                      help="source files to compile and run")
  args = parser.parse_args()

  state = CompState(args.interface, args.verbose)
  # compile stdlib
  stdlib("logo/lib.logo", state)
  for fn in args.sources:
    if fn == "-":
      return
    load(fn, state)
  # run repl
  repl(args, state)


if __name__ == "__main__":
  main()
