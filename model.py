#!/usr/bin/env python

import pyglet
from pyglet import gl, window
import math


class Window(window.Window):
  def __init__(self):
    window.Window.__init__(self)
    pyglet.clock.schedule(self.update)
    self.t = 0
    self.angles = [0]*4
    self.up = [False]*4
    self.selected = -1
    self.plots = [[] for i in range(8)]
                 
  def on_mouse_scroll(self, x, y, scroll_x, scroll_y):
    self.zoom += scroll_y

  def on_mouse_press(self, x, y, buttons, mods):
    x -= 320
    y -= 240
    if y > 0:
      self.selected = 0 if x > 0 else 1
    else:
      self.selected = 2 if x > 0 else 3

  def on_mouse_release(self, x, y, buttons, mods):
    self.selected = -1

  def on_mouse_drag(self, x, y, dx, dy, buttons, modifiers):
    if self.selected == -1:
      return
    sx, sy = [(1,1),(-1,1),(1,-1),(-1,-1)][self.selected]

    cx = 320 + (14*3*sx)
    cy = 240 + (12*3*sy)

    ox = x - cx
    oy = y - cy

    angle = math.atan2(oy*sy, ox*sx)/math.pi*180.
    self.angles[self.selected] = angle

  def on_key_press(self, sym, modifiers):
    if sym == window.key._1:
      self.up[0] = not self.up[0]
    elif sym == window.key._2:
      self.up[1] = not self.up[1]
    elif sym == window.key._3:
      self.up[2] = not self.up[2]
    elif sym == window.key._4:
      self.up[3] = not self.up[3]
    else:
      window.Window.on_key_press(self, sym, modifiers)

  def update(self, dt):
    self.t += dt

  def _draw_circle(self, x, y, r):
    gl.glBegin(gl.GL_TRIANGLE_FAN)
    gl.glVertex2f(x,y)
    for i in range(11):
      a = math.pi*2/10.*i
      i,j = math.sin(a)*r, math.cos(a)*r
      gl.glVertex2f(x+i,y+j)
    gl.glEnd()

  def _draw_leg(self, mx, my, angle, sel):
    lcol = (1,0,0) if sel else (1,1,1)

    gl.glPushMatrix()
    gl.glScalef(mx,my,0)
    gl.glTranslatef(14, 12, 0)

    gl.glRotatef(angle*my, 0,0,1)

    gl.glColor3f(1,0,1)
    self._draw_circle(0, 0, 2)
    # shoulder servo
    gl.glBegin(gl.GL_LINE_LOOP)
    gl.glVertex2f(-6, -11)
    gl.glVertex2f(-6,  21)
    gl.glVertex2f( 6,  21)
    gl.glVertex2f( 6, -11)
    gl.glEnd()
    # knee servo
    gl.glBegin(gl.GL_LINE_LOOP)
    gl.glVertex2f(-6 + 12, -6)
    gl.glVertex2f(-6 + 12,  16)
    gl.glVertex2f( 6 + 12,  16)
    gl.glVertex2f( 6 + 12, -6)
    gl.glEnd()

    gl.glColor3f(*lcol)
    gl.glBegin(gl.GL_LINE_STRIP)
    gl.glVertex2f(12, 15)
    gl.glVertex2f(12, 25)
    gl.glVertex2f(37, 25)
    gl.glEnd()

    self._draw_circle(37, 25, 2)
    gl.glPopMatrix()

  def _calc_pos(self):
    poses = []
    for joint in [0,1,3,2]:
      if self.up[joint]:
        continue
      sx, sy = [(1,1),(-1,1),(1,-1),(-1,-1)][joint]
      a = (self.angles[joint]/180.)*math.pi
      a *= (sx)
      dx = 37*sx
      dy = 25*sy
      x = dx*math.cos(a) - dy*math.sin(a)
      y = dx*math.sin(a) + dy*math.cos(a)
      x += 14*sx
      y += 12*sy
      poses.append((x,y))
    return poses

  def xgait(self, t, leg):
    t = (t*4 - (leg*4))%16
    up = 0.2 <= t <= 3.8
    ang = 0
    da = 50.
    if t < 4:
      ang = (t/4.)*da
    else:
      x = (t-4)/12.
      ang = (1-x)*da
    return up, ang-da/2.

  def gait(self, t, leg):
    t = (t*8)%20
    legmin = leg*4
    legmax = (leg+1)*4
    up = legmin <= t <= legmax
    ang = 0
    da = 20.
    sign = [1,-1,-1,1][leg]
    if legmin <= t <= legmax:
      ang = ((t-legmin)/4.)*da*sign
    elif t <= 16:
      if t >= leg*4:
        ang = da*sign
    else:
      ang = (1 - ((t-16)/4.)) * da*sign
    return up, ang

  def on_draw(self):
    gl.glClear(gl.GL_COLOR_BUFFER_BIT|gl.GL_DEPTH_BUFFER_BIT)
    gl.glLoadIdentity()

    gl.glPushMatrix()

    gl.glTranslatef(320,240,0)
    gl.glScalef(3,3,0)
    gl.glColor3f(1,1,1)
    self._draw_circle(0, 0, 2)
    gl.glBegin(gl.GL_LINE_LOOP)
    gl.glVertex2f(-24,-22)
    gl.glVertex2f( 24,-22)
    gl.glVertex2f( 24, 22)
    gl.glVertex2f(-24, 22)
    gl.glEnd()

    # fl br fr bl

    j = [1,2,0,3]
    for i in range(4):
      self.up[j[i]], self.angles[j[i]] = self.gait(self.t, i) #*4-(i*4))

    self._draw_leg( 1, 1, self.angles[0], self.selected==0) # fr
    self._draw_leg(-1, 1, self.angles[1], self.selected==1) # fl
    self._draw_leg( 1,-1, self.angles[2], self.selected==2) # br
    self._draw_leg(-1,-1, self.angles[3], self.selected==3) # bl

    for i in range(4):
      self.plots[i*2].insert(0,self.angles[i])
      self.plots[i*2+1].insert(0,self.up[i])

      self.plots[i*2] = self.plots[i*2][:320]
      self.plots[i*2+1] = self.plots[i*2+1][:320]

    gl.glColor3f(1,1,0)
    poses = self._calc_pos()
    gl.glBegin(gl.GL_LINE_LOOP)
    for px, py in poses:
      gl.glVertex2f(px, py)
    gl.glEnd()
    for px, py in poses:
      self._draw_circle(px, py, 1)

    gl.glPopMatrix()

    return

    for j, plot in enumerate(self.plots):
      hi, lo = max(plot), min(plot)
      scale = hi-lo
      if scale == 0:
        scale = 1
      gl.glBegin(gl.GL_LINE_STRIP)
      for i, v in enumerate(plot):
        px = i*2
        py = j*10 + ((v-lo)/float(scale))*5
        gl.glVertex2f(px, py)
      gl.glEnd()


def main():
  Window()
  pyglet.app.run()

main()

